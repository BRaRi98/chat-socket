USE [master]
GO
/****** Object:  Database [chat]    Script Date: 18/9/2021 14:24:11 ******/
CREATE DATABASE [chat]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'chat', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\chat.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'chat_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\chat_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [chat] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [chat].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [chat] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [chat] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [chat] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [chat] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [chat] SET ARITHABORT OFF 
GO
ALTER DATABASE [chat] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [chat] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [chat] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [chat] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [chat] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [chat] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [chat] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [chat] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [chat] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [chat] SET  ENABLE_BROKER 
GO
ALTER DATABASE [chat] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [chat] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [chat] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [chat] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [chat] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [chat] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [chat] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [chat] SET RECOVERY FULL 
GO
ALTER DATABASE [chat] SET  MULTI_USER 
GO
ALTER DATABASE [chat] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [chat] SET DB_CHAINING OFF 
GO
ALTER DATABASE [chat] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [chat] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [chat] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [chat] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'chat', N'ON'
GO
ALTER DATABASE [chat] SET QUERY_STORE = OFF
GO
USE [chat]
GO
/****** Object:  Table [dbo].[attachment]    Script Date: 18/9/2021 14:24:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[attachment](
	[attach_id] [int] IDENTITY(1,1) NOT NULL,
	[content_id] [int] NOT NULL,
	[attach] [varbinary](max) NOT NULL,
	[type] [nvarchar](50) NOT NULL,
	[attach_name] [varchar](40) NOT NULL,
	[attach_type] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[attach_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[attachment_type]    Script Date: 18/9/2021 14:24:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[attachment_type](
	[attachment_type_id] [int] IDENTITY(1,1) NOT NULL,
	[description] [nvarchar](30) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[attachment_type_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[connection_status]    Script Date: 18/9/2021 14:24:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[connection_status](
	[connection_status_id] [int] IDENTITY(1,1) NOT NULL,
	[description] [nvarchar](max) NOT NULL,
	[creation_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[connection_status_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[content]    Script Date: 18/9/2021 14:24:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[content](
	[content_id] [int] IDENTITY(1,1) NOT NULL,
	[message_id] [int] NOT NULL,
	[content] [nvarchar](max) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[content_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[conversation]    Script Date: 18/9/2021 14:24:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[conversation](
	[conversation_id] [int] IDENTITY(1,1) NOT NULL,
	[address] [nvarchar](20) NOT NULL,
	[status] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[conversation_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[message]    Script Date: 18/9/2021 14:24:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[message](
	[message_id] [int] IDENTITY(1,1) NOT NULL,
	[user_id] [int] NOT NULL,
	[conversation_id] [int] NOT NULL,
	[message_type_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[message_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[message_action]    Script Date: 18/9/2021 14:24:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[message_action](
	[message_action_id] [int] IDENTITY(1,1) NOT NULL,
	[description] [nvarchar](30) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[message_action_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[message_agenda]    Script Date: 18/9/2021 14:24:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[message_agenda](
	[message_id] [int] NOT NULL,
	[message_action_id] [int] NOT NULL,
	[action_date] [datetime] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[message_type]    Script Date: 18/9/2021 14:24:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[message_type](
	[message_type_id] [int] IDENTITY(1,1) NOT NULL,
	[description] [nvarchar](30) NOT NULL,
	[status] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[message_type_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user]    Script Date: 18/9/2021 14:24:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user](
	[user_id] [int] IDENTITY(1,1) NOT NULL,
	[nickname] [nvarchar](20) NOT NULL,
	[pwd] [nvarchar](200) NOT NULL,
	[socket_id] [nvarchar](max) NOT NULL,
	[user_status_id] [int] NULL,
	[connection_status_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[user_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_status]    Script Date: 18/9/2021 14:24:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_status](
	[user_status_id] [int] IDENTITY(1,1) NOT NULL,
	[description] [nvarchar](max) NOT NULL,
	[creation_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[user_status_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_token]    Script Date: 18/9/2021 14:24:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_token](
	[user_token_id] [int] IDENTITY(1,1) NOT NULL,
	[refresh_token] [nvarchar](max) NOT NULL,
	[creation_date] [datetime] NULL,
	[expiration_date] [datetime] NULL,
	[active] [bit] NULL,
	[user_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[user_token_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[connection_status] ADD  DEFAULT (getdate()) FOR [creation_date]
GO
ALTER TABLE [dbo].[conversation] ADD  DEFAULT ((1)) FOR [status]
GO
ALTER TABLE [dbo].[message_type] ADD  DEFAULT ((1)) FOR [status]
GO
ALTER TABLE [dbo].[user] ADD  DEFAULT ((1)) FOR [user_status_id]
GO
ALTER TABLE [dbo].[user] ADD  DEFAULT ((2)) FOR [connection_status_id]
GO
ALTER TABLE [dbo].[user_status] ADD  DEFAULT (getdate()) FOR [creation_date]
GO
ALTER TABLE [dbo].[user_token] ADD  DEFAULT (getdate()) FOR [creation_date]
GO
ALTER TABLE [dbo].[user_token] ADD  DEFAULT ((1)) FOR [active]
GO
ALTER TABLE [dbo].[attachment]  WITH CHECK ADD  CONSTRAINT [fk_attach_content_id] FOREIGN KEY([content_id])
REFERENCES [dbo].[content] ([content_id])
GO
ALTER TABLE [dbo].[attachment] CHECK CONSTRAINT [fk_attach_content_id]
GO
ALTER TABLE [dbo].[attachment]  WITH CHECK ADD  CONSTRAINT [fk_attachment_attachment_type_id] FOREIGN KEY([attach_type])
REFERENCES [dbo].[attachment_type] ([attachment_type_id])
GO
ALTER TABLE [dbo].[attachment] CHECK CONSTRAINT [fk_attachment_attachment_type_id]
GO
ALTER TABLE [dbo].[content]  WITH CHECK ADD  CONSTRAINT [fk_content_message_id] FOREIGN KEY([message_id])
REFERENCES [dbo].[message] ([message_id])
GO
ALTER TABLE [dbo].[content] CHECK CONSTRAINT [fk_content_message_id]
GO
ALTER TABLE [dbo].[message]  WITH CHECK ADD  CONSTRAINT [fk_message_conversation_id] FOREIGN KEY([conversation_id])
REFERENCES [dbo].[conversation] ([conversation_id])
GO
ALTER TABLE [dbo].[message] CHECK CONSTRAINT [fk_message_conversation_id]
GO
ALTER TABLE [dbo].[message]  WITH CHECK ADD  CONSTRAINT [fk_message_message_type_id] FOREIGN KEY([message_type_id])
REFERENCES [dbo].[message_type] ([message_type_id])
GO
ALTER TABLE [dbo].[message] CHECK CONSTRAINT [fk_message_message_type_id]
GO
ALTER TABLE [dbo].[message]  WITH CHECK ADD  CONSTRAINT [fk_message_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[user] ([user_id])
GO
ALTER TABLE [dbo].[message] CHECK CONSTRAINT [fk_message_user_id]
GO
ALTER TABLE [dbo].[message_agenda]  WITH CHECK ADD  CONSTRAINT [fk_message_agenda_message_action_id] FOREIGN KEY([message_action_id])
REFERENCES [dbo].[message_action] ([message_action_id])
GO
ALTER TABLE [dbo].[message_agenda] CHECK CONSTRAINT [fk_message_agenda_message_action_id]
GO
ALTER TABLE [dbo].[message_agenda]  WITH CHECK ADD  CONSTRAINT [fk_message_agenda_message_id] FOREIGN KEY([message_id])
REFERENCES [dbo].[message] ([message_id])
GO
ALTER TABLE [dbo].[message_agenda] CHECK CONSTRAINT [fk_message_agenda_message_id]
GO
ALTER TABLE [dbo].[user]  WITH CHECK ADD  CONSTRAINT [fk_us_to_ustatus] FOREIGN KEY([user_status_id])
REFERENCES [dbo].[user_status] ([user_status_id])
GO
ALTER TABLE [dbo].[user] CHECK CONSTRAINT [fk_us_to_ustatus]
GO
ALTER TABLE [dbo].[user_token]  WITH CHECK ADD  CONSTRAINT [fk_ut_to_us] FOREIGN KEY([user_id])
REFERENCES [dbo].[user] ([user_id])
GO
ALTER TABLE [dbo].[user_token] CHECK CONSTRAINT [fk_ut_to_us]
GO
/****** Object:  StoredProcedure [dbo].[auth_sp_insert_user]    Script Date: 18/9/2021 14:24:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Benjamín Ramírez>
-- Create date: <11 de Septiembre de 2021>
-- Description:	<Procedimiento almacenado que inserta un usuario>
-- =============================================
CREATE PROCEDURE [dbo].[auth_sp_insert_user]
	-- Add the parameters for the stored procedure here
	@nickname nvarchar(40),
	@pwd nvarchar(400)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	insert into [user] ([nickname], [pwd]) values (@nickname, @pwd)
END
GO
/****** Object:  StoredProcedure [dbo].[auth_sp_logged]    Script Date: 18/9/2021 14:24:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Benjamín Ramírez>
-- Create date: <11 de Septimebre de 2021>
-- Description:	<Procedimiento almacenado que verifica si un usuario ya está logueado en la base de datos y devolviendo su refresh JWT>
-- =============================================
CREATE PROCEDURE [dbo].[auth_sp_logged]
	-- Add the parameters for the stored procedure here
	@user_id integer
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	select [refresh_token] from [user_token] where [user_id] = @user_id and [active] = 1
END
GO
/****** Object:  StoredProcedure [dbo].[auth_sp_login]    Script Date: 18/9/2021 14:24:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Benjamín Ramírez>
-- Create date: <11 de Septiembre de 2021>
-- Description:	<Procedimiento loguea a un usuario>
-- =============================================
CREATE PROCEDURE [dbo].[auth_sp_login]
	-- Add the parameters for the stored procedure here
	@nickname as nvarchar(40),
	@pwd as nvarchar(200)
AS
BEGIN
	select * from [user] where [nickname] like @nickname and [pwd] like @pwd
END
GO
/****** Object:  StoredProcedure [dbo].[auth_sp_login_store_token]    Script Date: 18/9/2021 14:24:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Benjamín Ramírez>
-- Create date: <11 de Septiembre de 2021>
-- Description:	<Procedimiento que loguea al usuario dentro de la base de datos con un token en user_token>
-- =============================================
CREATE PROCEDURE [dbo].[auth_sp_login_store_token]
	-- Add the parameters for the stored procedure here
	@user_id integer,
	@refresh_token nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	insert into [user_token] ([refresh_token], [user_id]) values (@refresh_token, @user_id)
END
GO
/****** Object:  StoredProcedure [dbo].[auth_sp_return_credentials_by_nickname]    Script Date: 18/9/2021 14:24:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Benjamín Ramírez>
-- Create date: <15 de Septiembre de 2021>
-- Description:	<Este procedimiento almacenado retorna las credenciales de un usuario en base a su nickname>
-- =============================================
CREATE PROCEDURE [dbo].[auth_sp_return_credentials_by_nickname]
	-- Add the parameters for the stored procedure here
	@nickname nvarchar(40)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	select [nickname], [pwd] from [user] where [nickname] like @nickname
END
GO
/****** Object:  StoredProcedure [dbo].[auth_sp_return_unavailable_nicknames]    Script Date: 18/9/2021 14:24:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Benjamín Ramírez>
-- Create date: <18 de Septiembre 2021>
-- Description:	<Procedimiento que retorna todos los nicknames ingresados a la base de datos>
-- =============================================
CREATE PROCEDURE [dbo].[auth_sp_return_unavailable_nicknames]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	select [nickname] from [user]
END
GO
/****** Object:  StoredProcedure [dbo].[auth_sp_return_user_by_nickname]    Script Date: 18/9/2021 14:24:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Benjamín Ramírez>
-- Create date: <11 de Septiembre de 2021>
-- Description:	<Procedimiento almacenado que retorna el id de un usuario realizando una búsqueda a partir de su nickname>
-- =============================================
CREATE PROCEDURE [dbo].[auth_sp_return_user_by_nickname]
	-- Add the parameters for the stored procedure here
	@nickname as nvarchar(40)
AS
BEGIN
	select [user_id] from [user] where [nickname] like @nickname
END
GO
USE [master]
GO
ALTER DATABASE [chat] SET  READ_WRITE 
GO
