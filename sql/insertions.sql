use chat;
insert into [message_action] ([description]) values ('sent'), ('read'), ('deleted') 
insert into [message_type] ([description]) values ('text'), ('attachment'), ('text with attachment')
insert into [attachment_type] ([description]) values ('image'), ('file')
insert into [user_status] ([description]) values ('active'), ('inactive')
insert into [connection_status] ([description]) values ('online'), ('offline'), ('bussy'), ('available')