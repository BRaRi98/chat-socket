"use strict";

//Libraries
const db = require( "./db" );
const mssql = require( "mssql" );
const { encrypter } = require( "../services" );

//Destructuring utils functions and variables
const {
    compare,
    encrypt
} = encrypter;

//This function insert an user in the database from an object
const inUser = async user => {
    try {
        const cnx = db.getConnection();
        const request = cnx.request();
        request.input( "@nickname", mssql.NVarChar, user.nickname );
        request.input( "@pwd", mssql.NVarChar, encrypt( user.pwd, 1 ) );
        return await request.execute( "auth_sp_insert_user" );   
    } catch ( error ) {
        console.log( error );
    }
}

//This function log an user in the database
const login = async user => {
    try {
        const cnx = db.getConnection();
        const request = cnx.request();
        const searched = searchCredentialsByNickname( user );
        let credentials;
    
        if ( searched[0] ) { return searched } else { credentials = searched[1] };
    
        const matchingPwd = await compare( credentials[0].pwd, user.pwd );
    
        if ( matchingPwd ) { return [ false, "Incorrect Password" ] };
    
        request.input( "@nickname", mssql.NVarChar, user.nickname );
        request.input( "@pwd", mssql.NVarChar, user.pwd );
        const resultLogin = await request.execute( "auth_sp_login" );
        const recordLogin = resultLogin.recordset;
    
        return [ true, recordLogin ];
    } catch ( error ) {
        console.log( error );
    }
}

//This function search an user by nickname
const searchCredentialsByNickname = async user =>{
    try {
        const cnx = db.getConnection();
        const request = cnx.request();
        request.input( "@nickname", mssql.NVarChar, user.nickname );
        const resultSearch = await request.execute( "auth_sp_return_credentials_by_nickname" );
        const credentials = resultSearch.recordset;
        if ( credentials.length == 0 ) return [ false, "User not found" ];
        return [ true, credentials ];
    } catch ( error ) {
        console.log( error );
    }
}

//Exporting
module.exports = {
    inUser,
    login,
    searchCredentialsByNickname
}