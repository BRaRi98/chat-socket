"use strict";

//Requiring all models
const user = require( "./user" );

//Exporting
module.exports = {
    user
}