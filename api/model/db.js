"use strict";

//Libraries
const mssql = require( "mssql" );
const { sql } = require( "../config" );
const colors = require( "colors" );

//Global variables
let pool = null; //pool for connection

//This function asigns "pool" value to null
const closePool = async () => {
    try{
        await pool.close();
    } catch ( err ) {
        pool = null;
        console.log(err);;
    }
};

//This functiong connect and return pool using "mssql" and "sql" config object
const getConnection = async () => {
    try {
        if ( pool ) return pool;
        pool = await mssql.connect( sql );
        pool.on( 'error', async err => {
            console.log( err.message.red );
            await closePool();
        } );
        return pool;
    } catch ( err ) {
        console.log( err.message.red );
        pool = null;
    }
};

//Exporting
module.exports = {
    closePool,
    getConnection
}