"use strict";

const router = require( "express" ).Router();
const { user } = require( "../controller" );

const { 
    routes
} = user;

const { 
    login
} = routes

router
    .route( '/login' )
    .post( login );

module.exports = router;