"use strict";

const bcrypt = require( "bcrypt" );
const { auth: { saltsPwd } } = require( "../config" );

const encrypt = async ( toEncrypt, context ) => {
    try {
        let salts = 0;

        switch ( context ) {
            case 1:
                salts = saltsPwd;
                break;
            default:
                salts = saltsPwd;
                break;
        }

        return await bcrypt.hashSync( toEncrypt, parseInt( salts ) );
    } catch (error) {
        console.log(error.message);
    }
}

const compare = async ( toCompare, comparison ) => {
    return await bcrypt.compareSync( toCompare, comparison );
}

module.exports = {
    encrypt,
    compare
}