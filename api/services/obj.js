"use strict";

//This service contains all util functions for Objects

//This function run a callback for every property in object, it receives object< object > and callback< function, arrowfunction > parameter
const throughObj = ( object, callback ) => {
    for( let property in object ) callback( property, object );
};

//Exporting
module.exports = {
    throughObj
}