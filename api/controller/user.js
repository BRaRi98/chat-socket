"use strict";

//Requiring configurations and models
const { user } = require( "../model" );
const colors = require( "colors" );

//Destructuring
const { searchCredentialsByNickname } = user;

const routes = {
    login: async ( req, res ) => {
        try{
            const credentials = searchCredentialsByNickname( req.params );
            res.json( {
                success: credentials[0],
                status: ( credentials[0] ) ? 200 : 404,
                msg: (credentials[0]) ? "User found" : "User not found"
            } );
        } catch ( error ) {
            console.log( error.message.red );
        }
    }
}

module.exports = {
    routes
}
