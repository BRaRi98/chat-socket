"use strict";

//Libraries
const dotenv = require( 'dotenv' );
const assert = require( 'assert' );

//Configuring .env into process.env
dotenv.config()

//Getting all .env variables
const {
    SOCKET_CORS_ORIGIN,
    SOCKET_CORS_METHODS,
    SERVER_PORT,
    SERVER_ROUTE,
    SERVER_ENV,
    SERVER_CORS_ORIGIN,
    SERVER_CORS_METHODS,
    SQL_SERVER,
    SQL_DATABASE,
    SQL_USER,
    SQL_PWD,
    OTHER_SALTS_PWD
} = process.env;

//Config Skeleton Object for assertions
const configSkeleton = {
    SOCKET_CORS_ORIGIN,
    SOCKET_CORS_METHODS,
    SERVER_PORT,
    SERVER_ROUTE,
    SERVER_ENV,
    SERVER_CORS_ORIGIN,
    SERVER_CORS_METHODS,
    SQL_SERVER,
    SQL_DATABASE,
    SQL_USER,
    SQL_PWD,
    OTHER_SALTS_PWD
};

//Asserting missing variables
for (let property in configSkeleton) {
    assert( configSkeleton[ property ], `${ property } missing on .env file` )
}

//Exporting
module.exports = {
    socket: {
        cors: {
            origin: SOCKET_CORS_ORIGIN,
            methods: SOCKET_CORS_METHODS
        }
    },
    server: {
        port: SERVER_PORT,
        route: SERVER_ROUTE,
        env: SERVER_ENV,
        cors: {
            origin: SERVER_CORS_ORIGIN,
            methods: SERVER_CORS_METHODS
        }
    },
    sql: {
        server: SQL_SERVER,
        database: SQL_DATABASE,
        user: SQL_USER,
        password: SQL_PWD,
        options: {
            encrypt: false,
            enableArithAbort: false
        }
    },
    auth: {
        saltsPwd: OTHER_SALTS_PWD
    }
};