"use strict";

//Loading Configurations
const { server: serverConfig, socket: socketConfig } = require( './config' );

//Libraries
const express = require( 'express' );
const { createServer } = require( 'http' );
const morgan = require( 'morgan' );
const ioSetup = require( 'socket.io' );
const cors = require( 'cors' );
const colors = require( 'colors' );

//Required modules
const routes = require( './route' );

//Destructuring
const { cors: serverCors, port, env, route } = serverConfig;
const { cors: socketCors } = socketConfig;
const { json } = express;
const { user } = routes;

//Setups
const app = express();
const server = createServer( app );
const io = ioSetup( server, socketCors );

//Main
const main = () => {
    //JSON Body Parser
    app.use( json() );
    
    //CORS
    app.use( cors( serverCors ) );

    //User routes
    app.use( '/auth', user );

    //Setting dev middleware for watching routes on console
    ( env === 'dev' ) && app.use( morgan( 'dev' ) );

    //Mounting server
    server.listen( port, console.log( `Environment: ${ env } | Server running at: ${ route + `:` + port }`.blue ) );
}

//Running main();
main();